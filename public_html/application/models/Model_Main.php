<?php

/**
 * Created by PhpStorm.
 * User: hillel
 * Date: 17.12.17
 * Time: 17:48
 */
class Model_Main extends Model
{
    public function getContacts()
    {
        $contacts = [
            'phone' => '123-12-12',
            'address' => 'B.Arnautska street',
            'email' => 'super@gmail.com'
        ];
        return $contacts;
    }
}