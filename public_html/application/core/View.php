<?php

/**
 * Created by PhpStorm.
 * User: hillel
 * Date: 17.12.17
 * Time: 17:19
 */
class View
{
    public function generate($contentView, $data = [], $templateView = 'template_view.php'){
        include 'application/views/'.$templateView;
    }
}